var http = require('http');
var fs = require('fs');
var disconnect = new String;
server = http.createServer(function(req, res) {});
server.listen('8081');

var nowjs = require("now");
var everyone = nowjs.initialize(server);

var users = [];
nowjs.on('connect', function() {
	this.now.setUsername();

	//
});

nowjs.on('disconnect', function() {
	disconnect = users[this.user.clientId].username;
  for ( var i  in  users[this.user.clientId].groups){
    gname = users[this.user.clientId].groups[i].groupName;
    console.log(gname);
  }
	everyone.now.offline(disconnect);
	delete users[this.user.clientId];
});

everyone.now.setUsername = function() {
	users[this.user.clientId] = {
		username: this.now.name,
		id: this.user.clientId
	};
	numusers = 0;
	nowjs.getGroup('everyone').count(function(count) { //  only broadcast to all users  if there is more than  one user connect
		numusers = count;
	});
	if (numusers > 1) {
		everyone.now.online(users[this.user.clientId].username);
	}

};

everyone.now.checkMyFriend = function(friend, callback) {
	var isOnline = false;
	for (var i in users) {
		if (users[i].username == friend) {
			isOnline = true;
			break;
		}
	}
	callback(isOnline, friend);
};

everyone.now.distributeMessage = function(msg, room) {
	//console.log(room);
	var group = nowjs.getGroup(this.now.serverRoom);
	//console.log(this.now.serverRoom);
	group.now.receiveMessage(this.now.name, msg);
};

everyone.now.addFriendToChat = function(chatroom, sender, receiver, callback) {
	//check here to see if the receiver  is already chatting by using nowjs.getClient  and  user.getGroups
	var user;
	var thisGroups;
	var isBusy = false;
	var newGroup;
	for (var i in users) {
		if (users[i].username == receiver) {
			user = nowjs.getClient(users[i].id, function() {
				thisGroups = this.getGroups(function(groups) {
					if (groups.length > 1) {
						isBusy = true;
					}
				});
			});
			if (isBusy == true) {
				callback(true);
			} else {
				newGroup = nowjs.getGroup(chatroom);
				this.now.serverRoom = newGroup.groupName;
				newGroup.addUser(users[i].id);
				newGroup.addUser(this.user.clientId);


				newGroup.now.startChat(newGroup.groupName);
				newGroup.now.receiveMessage('system', users[i].username + ' has joined the chat');
				newGroup.now.receiveMessage('system', sender + ' has started a chat with you');
				callback(false);
			}
		}
	}
	newGroup.on('leave', function(){
     everyone.now.closeMessages(this.now.serverRoom, this.now.name);
  });
};

everyone.now.closeMessages = function(room, user) {
	var Group = nowjs.getGroup(room);
	Group.now.receiveMessage('system', user + ' has initiated shutdown  of this chat');
	Group.now.receiveMessage('system', ' this chat will close in three seconds.');

	setTimeout(function() {
		Group.now.closeChat();
		Group.getUsers(function(users) {
			for (var i = 0; i < users.length; i++) {
				Group.removeUser(users[i]);
			};
		});
	},
	3000);
};


