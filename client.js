;(function($) {
  $(document).ready( function(){
	  $('body').append('<div id=\"friendlist\" class=\"chatwrapper\"><h3>Chat</h3><div class=\"chat\"></div></div>');
	  $('.chat').html(Drupal.settings.following_list['following_list']);
	  $('#friendlist h3').click( function(e){
		  console.log(e);
	    $(this).next().toggle();
	  });
	  	 
	  
	  $('.chat .view-content .item-list  li a').click( function(e){
	    if($(this).parents('li').css('background-image') ){
	      
	    }
	    e.preventDefault();
	    uname = $(this).text();
	    if($('.chatwrapper').length == 1){
	      now.serverRoom  = Drupal.settings.username['username'] + '+' +  uname;
	      now.addFriendToChat(Drupal.settings.username['username'] + '+' +  uname, Drupal.settings.username['username'] , uname, function(busy){
	        if(busy == true){
	          alert(uname + '  is chatting with another user.  Try again in a few minutes');
	        }
	      });
      }
      else {
        alert('We only support one chat at a time');
      }
	  });
	  
	  now.ready(function(){
      	now.name = Drupal.settings.username['username']; // variable advertise that this user is available for chat.
      	now.setUsername();
      	$('.chat .view-content .item-list  li a').each( function(){
        	friend = $(this).text();
        	frienda = $(this);
        	now.checkMyFriend(friend, function(isOnline, user){
          	setTimeout( function(){
            	if(isOnline == true) {
              	  redlight_greenlight('green', 'red', user);
              	$(this).attr('href', '#');
            	}
            	else {
              	  redlight_greenlight('red', 'green', user);
              	$(this).removeAttr('href');
            	}
          	  }, 500);
        	});
      });
      
      now.online = function(username){
        setTimeout( function(){
          $('.chat').find('li a').each( function(){
            if( $(this).text() === username) {
              redlight_greenlight('green', 'red', $(this).text());
              $(this).attr('href', '#');
            }
          });
        }, 500 );
      };
      
      now.offline = function(username){
        setTimeout( function(){
          $('.chat').find('li a').each( function(){
            if($(this).text() == username) {
              redlight_greenlight('red', 'green', $(this).text());
              $(this).removeAttr('href');
            }
          });
        }, 500 );
      };

   });

   now.receiveMessage = function(name, msg) {
     $("<div></div>").text(name + ': ' + msg).appendTo('.currentChat .chat');
     $(".currentChat .chat").animate({ scrollTop: $(".currentChat .chat").attr("scrollHeight") }, 500);
   };
   
   now.startChat  = function(room){
     numchats = $('.chatwrapper').length;
     console.log('There are ' + numchats + ' chat windows');
     if(numchats > 1){
				return;
     }
     console.log('chat started');
     now.serverRoom  = room;
     context = $('.chatwrapper').clone().appendTo('body').addClass('currentChat').get(0);
     $('.currentChat .chat').empty();
     $('.currentChat').removeAttr('id').append('<div class=\'submitwrapper\'><input class=\'submittext\' type=\'textarea\'/><input class=\'send\' type=\'button\' value=\'send\'/></div>');
     $('.currentChat').append('<div class=\'close\'><a href="#">Close</a></div> ');
     $('.currentChat').show();   
     Drupal.attachBehaviors(context); 
     $('.currentChat .chat').show();
   };
   
   now.closeChat = function(){
     $(".currentChat .chat").animate({ scrollTop: $(".currentChat .chat").attr("scrollHeight") }, 500);
     //sleep(3000);
     $('.currentChat').remove();
     //Drupal.attachBehaviors('.chatwrapper'); 
    };
  });
  
  Drupal.behaviors.sendMessage = function (context) {
      $('.send', context).click( function(context){
        msg = $('.submittext').attr('value');
        now.distributeMessage(msg, now.serverRoom);
        $('.submittext').attr('value', '');
       });
	   
       $(".submittext", context).keypress(function(event) {
         if ( event.which == 13 ) {
           $('.send').click();
         }
       });
	   
	
	}
  
    Drupal.behaviors.closeChatWindow = function(context){
        $('.close a').click( function(e){
          e.preventDefault();
          now.closeMessages(now.serverRoom, Drupal.settings.username['username']);
        });
	
	
	};
  
    Drupal.behaviors.toggleChatWindow = function(context){
        $('.currentChat h3').click( function(e){
    	    $(this).parents('.currentChat').find('.chat').toggle();
    	    $(this).parents('.currentChat').find('.submitwrapper').toggle();
    	 });
      
	
	
	};
  
   
  
  function redlight_greenlight(colortochangeto, colortochangefrom, user){
     obj =  $('.chat .view-content .item-list  li a').each( function(){
       if($(this).text() == user){
         bg = $(this).parents('li').css('background-image');
         re = new RegExp(colortochangefrom); 
         newbg = bg.replace(re, colortochangeto);
         $(this).parents('li').css('background-image', newbg);
       }
     });

  }
})(jQuery);

function sleep(ms) {
  ms += new Date().getTime();
  while (new Date() < ms){}
}


